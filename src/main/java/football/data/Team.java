package football.data;

import javax.persistence.Embeddable;

@Embeddable
public class Team {
	
	
private  int teamId;
private String TeamName;
private static int counter;

public Team() {
}

public Team(String TeamName) {
	super();
	findId();
	this.TeamName = TeamName;
}

public void setTeamId(int teamId) {
	this.teamId = teamId;
}

public int getTeamId() {
	return teamId;
}

public String getTeamName() {
	return TeamName;
}

public void setTeamName(String teamName) {
	TeamName = teamName;
}

private void findId() {
	teamId=counter;
	counter+=1;
}
@Override
public String toString() {
	return " id=" + teamId + ", name=" + TeamName ;
}
}

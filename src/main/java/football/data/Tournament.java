package football.data;

import javax.persistence.Embeddable;

@Embeddable
public class Tournament {

	private int tournamentId;
	private String tournamentName;
	private static int counter;

	public Tournament() {
	}

	public Tournament( String tournamentName) {
		super();
		findId();
		this.tournamentName = tournamentName;
	}
	
	public int getTournamentId() {
		return tournamentId;
	}
	
	public void setTournamentId(int tournamentId) {
		this.tournamentId = tournamentId;
	}
	public String getTournamentName() {
		return tournamentName;
	}

	public void setTournamentName(String tournamentName) {
		this.tournamentName = tournamentName;
	}

	private void findId() {
		tournamentId=counter;
		counter+=1;
	}
	@Override
	public String toString() {
		return "Tournament id=" + tournamentId + ", name=" + tournamentName ;
	}
}

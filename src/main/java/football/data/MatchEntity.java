package football.data;


import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import football.FileName;


@Entity
@Table (name="MATCHS")
public class MatchEntity {

	private Long matchId;
	private StatusMatch matchStatus;
	private Date start_date, kickoff_time;
	private Tournament tournament;
	private int score;
	private Team team1 ;
	private Team team2; 
	private FileName fileName;
	private String allMatch;
	
	public MatchEntity() {
		allMatch = "dataOrigin.txt";
	}
	
	
	// played match
	public MatchEntity(StatusMatch status, Date start_date, Tournament tournament, int score ,Team team1 , Team team2 ) {
		super();
		this.matchStatus = status;
		this.start_date = start_date;
		this.tournament = tournament;
		this.team1=team1;
		this.team2=team2;
		this.score = score;
//		setFileName(status);
//		writeToFileMatchDetails(allMatch);
//		writeToFileMatchDetails(this.fileName.name()+".txt");
		
			

	}
	
	

	// // upcoming match
	public MatchEntity(StatusMatch status, Date start_date, Date kickoff_time, Tournament tournament ,Team team1 , Team team2 ) {
		super();
		this.matchStatus = status;
		this.start_date = start_date;
		this.tournament = tournament;
		this.team1=team1;
		this.team2=team2;
		this.kickoff_time = kickoff_time;
		setFileName(status);
		writeToFileMatchDetails(allMatch);
		writeToFileMatchDetails(this.fileName.name()+".txt");
	
	}
	
	public void writeToFileMatchDetails(String file_name)    {
		Writer fileWriter ;
		try {
			fileWriter = new FileWriter(file_name, true);
			fileWriter.write("\nMatch\nTournament : " + this.tournament+"\n");
			fileWriter.write("Team1 :  " + this.team1 + " Team2:  " + this.team2+"\n");
			fileWriter.write("Match Satuts : " + this.matchStatus+"\n");
			if(this.getMatchStatus().equals(StatusMatch.UpComing)) {
				fileWriter.write("Kick Off Date :" + this.kickoff_time+"\n");
			}
			if(this.getMatchStatus().equals(StatusMatch.Played)) {
				fileWriter.write("Match Score : " + this.score +"\n");
				}
		
			fileWriter.close();
	
			
		} catch (FileNotFoundException | UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public FileName getFileName() {
		return fileName;
	}

	public void setFileName(StatusMatch statusMatch) {
		if(statusMatch.equals(StatusMatch.UpComing)) {
			this.fileName = FileName.result_upComing;
		}
		else {
			this.fileName = FileName.result_played;
		}
		
	}
	
	
	
	@Column (name="ID")
	@Id
	public Long getMatchId() {
		return matchId;
	}

	public void setMatchId(Long matchId) {
		this.matchId = matchId;
	}

	@Enumerated (EnumType.STRING)
	public StatusMatch getMatchStatus() {
		return matchStatus;
	}

	public void setMatchStatus(StatusMatch matchStatus) {
		this.matchStatus = matchStatus;
	}

	@Temporal(TemporalType.TIMESTAMP)
	public Date getStart_date() {
		return start_date;
	}

	public void setStart_date(Date start_date) {
		this.start_date = start_date;
	}

	@Embedded
	public Tournament getTournament() {
		return tournament;
	}

	public void setTournament(Tournament tournament) {
		this.tournament = tournament;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	
	@AttributeOverrides({
		@AttributeOverride(name = "teamId",column=@Column(name="teamId1")),
		@AttributeOverride(name = "teamName",column=@Column(name="teamName1"))
	})
	@Embedded
	public Team getTeam1() {
		return team1;
	}

	public void setTeam1(Team team1) {
		this.team1 = team1;
	}

	@AttributeOverrides({
		@AttributeOverride(name = "teamId",column=@Column(name="teamId2")),
		@AttributeOverride(name = "teamName",column=@Column(name="teamName2"))
	})
	@Embedded
	public Team getTeam2() {
		return team2;
	}

	public void setTeam2(Team team2) {
		this.team2 = team2;
	}

	@Temporal(TemporalType.TIMESTAMP)
	public Date getKickoff_time() {
		return kickoff_time;
	}

	public void setKickoff_time(Date kickoff_time) {
		this.kickoff_time = kickoff_time;
	}
	

	@Override
	public String toString() {
		return " Match \n matchStatus: " + matchStatus + "\n start_date: " + start_date + ", kickoff_time: " + kickoff_time
				+ "\n " + tournament + " \n score = " + score + "\n team1:" + team1 + ", team2:" + team2 ;
	}
	
}
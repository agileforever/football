package football.logic;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import football.dao.MatchDao;
import football.data.MatchEntity;

@Service
public class MatchServiceImpl implements MatchService {
	private MatchDao matches;
	
	
	
	@Autowired
	public MatchServiceImpl(MatchDao matches) {
		super();
		this.matches = matches;
	}

	@Override
	public List<MatchEntity> getMatches(String filter, String teamOrtournament, String status) {
		return matches.readAll();
	}
	
	

}

package football.logic;
import java.util.List;

import football.data.MatchEntity;;
public interface MatchService {
	
	public List<MatchEntity> getMatches (String filter, String teamOrtournament,String status);

}

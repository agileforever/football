package football.dao;

import java.util.List;
import football.data.MatchEntity;

public interface MatchDao {
	
	public MatchEntity create (MatchEntity match);
	public List<MatchEntity>readAll();

}

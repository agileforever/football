package football.dao.memory;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.stereotype.Repository;

import football.dao.MatchDao;
import football.data.MatchEntity;


@Repository
public class MemoryMatchDao implements MatchDao {

	private Map <Long,MatchEntity> matches;
	private AtomicLong nextId;
	
	
	
	public MemoryMatchDao() {
		super();
		this.matches = Collections.synchronizedMap(new HashMap<>());
		this.nextId = new AtomicLong(1L);
	}

	public MatchEntity create (MatchEntity match) {
		match.setMatchId(this.nextId.getAndIncrement());
		this.matches.put(match.getMatchId(), match);
		return match;
	}


	@Override
	public List<MatchEntity> readAll() {
		return new ArrayList<MatchEntity> (this.matches.values());
	}
	

}

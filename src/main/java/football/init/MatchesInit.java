package football.init;

import java.util.Date;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import football.dao.MatchDao;
import football.data.MatchEntity;
import football.data.StatusMatch;
import football.data.Team;
import football.data.Tournament;

@Component
public class MatchesInit  implements CommandLineRunner{
	
	
	private MatchDao dao;

	
	@Autowired
	public MatchesInit(MatchDao dao) {
		super();
		this.dao = dao;
	}


	@Override
	public void run(String... args) throws Exception {
	
		
		MatchEntity m1 = new MatchEntity(StatusMatch.Played,new Date(),new Tournament("afeka football"),2,new Team ("Afeka"),new Team ("Tel Aviv"));
		MatchEntity m2 = new MatchEntity(StatusMatch.UpComing,new Date(),new Tournament("afeka football"),2,new Team ("Afeka"),new Team ("HIT"));
		MatchEntity m3 = new MatchEntity(StatusMatch.UpComing,new Date(),new Tournament("afeka football"),2,new Team ("HIT"),new Team ("Tel Aviv"));
		
		dao.create(m1);
		dao.create(m2);
		dao.create(m3);
		
	}
	
	
	

}

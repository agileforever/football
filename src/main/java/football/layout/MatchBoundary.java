package football.layout;
import java.util.Date;

import football.data.MatchEntity;


public class MatchBoundary {
	private String matchStatus;
	private Date start_date, kickoff_time;
	private String tournament;
	private int score;
	private String team1 ;
	private String team2; 
	
	public MatchBoundary () {	
	}
	
	public MatchBoundary(String matchStatus, Date start_date, Date kickoff_time, String tournament, int score,
			String team1, String team2) {
		super();
		this.matchStatus = matchStatus;
		this.start_date = start_date;
		this.kickoff_time = kickoff_time;
		this.tournament = tournament;
		this.score = score;
		this.team1 = team1;
		this.team2 = team2;
	}

	public MatchBoundary (MatchEntity match) {	
		this.matchStatus = match.getMatchStatus().toString();
		this.start_date = match.getStart_date();
		this.kickoff_time = match.getKickoff_time();
		this.tournament = match.getTournament().toString();
		this.score = match.getScore();
		this.team1 = match.getTeam1().toString();
		this.team2 = match.getTeam2().toString();
	}

	public String getMatchStatus() {
		return matchStatus;
	}

	public void setMatchStatus(String matchStatus) {
		this.matchStatus = matchStatus;
	}

	public Date getStart_date() {
		return start_date;
	}

	public void setStart_date(Date start_date) {
		this.start_date = start_date;
	}

	public Date getKickoff_time() {
		return kickoff_time;
	}

	public void setKickoff_time(Date kickoff_time) {
		this.kickoff_time = kickoff_time;
	}

	public String getTournament() {
		return tournament;
	}

	public void setTournament(String tournament) {
		this.tournament = tournament;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public String getTeam1() {
		return team1;
	}

	public void setTeam1(String team1) {
		this.team1 = team1;
	}

	public String getTeam2() {
		return team2;
	}

	public void setTeam2(String team2) {
		this.team2 = team2;
	}
	
}

package football.layout;
import football.logic.MatchService;

import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MatchRestAPI {
	private MatchService matchService;

	@Autowired
	public MatchRestAPI(MatchService matchService) {
		super();
		this.matchService = matchService;
	}
		
	@RequestMapping(
			method=RequestMethod.GET,
			path="/match/tournament/{tournamentName}/{status}",
			produces=MediaType.APPLICATION_JSON_VALUE)
	public MatchBoundary[] retrieveMatchesByTournament (
			@PathVariable("tournamentName") String tournamentName,
			@PathVariable("status") String status){
		return matchService.getMatches("tournament",tournamentName, status)
				.stream()
				.map(MatchBoundary::new)
				.collect(Collectors.toList())
				.toArray(new MatchBoundary[0]);
	}
	
	@RequestMapping(
			method=RequestMethod.GET,
			path="/match/team/{teamName}/{status}",
			produces=MediaType.APPLICATION_JSON_VALUE)
	public MatchBoundary[] retrieveMatchesByTeam (
			@PathVariable("teamName") String teamName,
			@PathVariable("status") String status){
		return matchService.getMatches("team",teamName, status)
				.stream()
				.map(MatchBoundary::new)
				.collect(Collectors.toList())
				.toArray(new MatchBoundary[0]);
	}

}
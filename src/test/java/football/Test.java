package football;

import java.util.ArrayList;
import java.util.List;
import org.junit.Before;

import football.data.Team;
import football.data.Tournament;

import static org.junit.Assert.assertEquals;



public class Test{
	private List <Team> allTeam; 
	private List <Tournament> tournaments;
	
	@Before
	public void setup() {
		allTeam = new ArrayList<>();
		tournaments = new ArrayList<>();
	}
	
	@org.junit.Test
	public void crateNewTeam() {
		Team team1 = new Team( "team1");
		
		
		assertEquals(0, team1.getTeamId());
	
	}
	
	@org.junit.Test
	public void crateNewTurnament() {
		Tournament tournament1 = new Tournament("tournament1");
		
		// Check if the id of tournament1 is 0
		assertEquals(0, tournament1.getTournamentId());
	
	}
	
}
